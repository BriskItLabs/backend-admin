import request from '@/utils/request'

export function getRecipeList() {
  return request({
    url: '/getRecipeList',
    method: 'post',
    data: { params: {} }
  })
}

export function getRecipeDetail(id) {
  return request({
    url: '/getRecipeDetail',
    method: 'post',
    data: { recipe_id: id }
  })
}

export function getRecipeStep(id) {
  return request({
    url: '/getRecipeStep',
    method: 'post',
    data: { recipe_id: id }
  })
}

export function setStep(id, params) {
  return request({
    url: '/setStep',
    method: 'post',
    data: { step_id: id, params: params }
  })
}

export function delStep(id) {
  return request({
    url: '/delStep',
    method: 'post',
    data: { step_id: id }
  })
}

export function addStep(id, params) {
  return request({
    url: '/addStep',
    method: 'post',
    data: { recipe_id: id, params: params }
  })
}


export function getRecipeServing(id) {
  return request({
    url: '/getRecipeServing',
    method: 'post',
    data: { recipe_id: id }
  })
}

export function addServing(id,params) {
  return request({
    url: '/addServing',
    method: 'post',
    data: { recipe_id: id,params }
  })
}

export function delServing(id) {
  return request({
    url: '/delServing',
    method: 'post',
    data: { serving_id: id }
  })
}

export function setServing(id,params) {
  return request({
    url: '/setServing',
    method: 'post',
    data: { serving_id: id,params }
  })
}


export function addRecipe(params) {
  return request({
    url: '/addRecipe',
    method: 'post',
    data: { params }
  })
}

export function setRecipe(id, params) {
  return request({
    url: '/setRecipe',
    method: 'post',
    data: { recipe_id: id, params }
  })
}

export function delRecipe(id) {
  return request({
    url: '/delRecipe',
    method: 'post',
    data: { recipe_id: id }
  })
}

export function getRecipeProgram(id) {
  return request({
    url: '/getRecipeProgram',
    method: 'post',
    data: { recipe_id: id }
  })
}

export function addProgram(id, params) {
  return request({
    url: '/addProgram',
    method: 'post',
    data: { recipe_id: id, params }
  })
}

export function updateProgram(id, params) {
  return request({
    url: '/setProgram',
    method: 'post',
    data: { program_id: id, params }
  })
}

export function delProgram(id) {
  return request({
    url: '/delProgram',
    method: 'post',
    data: { program_id: id }
  })
}

export function getExploreRecipe() {
  return request({
    url: '/getExploreRecipe',
    method: 'post',
    data: {}
  })
}

export function addLabel({ name, recipe_ids, index }) {
  return request({
    url: '/addLabel',
    method: 'post',
    data: { name, recipe_ids, index }
  })
}

export function delLabel(label_id) {
  return request({
    url: '/delLabel',
    method: 'post',
    data: { label_id }
  })
}

export function setLabel(label_id, name) {
  return request({
    url: '/setLabel',
    method: 'post',
    data: { label_id, params: { name } }
  })
}

export function addLabelRecipe({ label_id, recipe_ids, index }) {
  return request({
    url: '/addLabelRecipe',
    method: 'post',
    data: { label_id, recipe_ids, index }
  })
}

export function delLabelRecipe(label_id, recipe_ids) {
  return request({
    url: '/delLabelRecipe',
    method: 'post',
    data: { label_id, recipe_ids }
  })
}

export function setLabelRecipe( { label_id, recipe_ids }) {
  return request({
    url: '/setLabelRecipe',
    method: 'post',
    data: { label_id, recipe_ids }
  })
}

export function sortRecipe(label_id, index, flag) {
  return request({
    url: '/sortRecipe',
    method: 'post',
    data: { label_id, index, flag }
  })
}
