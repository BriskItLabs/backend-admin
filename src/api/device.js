import request from '@/utils/request'

export function getAllGrill() {
    return request({
        url: '/getAllGrill',
        method: 'post',
    })
}

export function getGrillDetail(id) {
    return request({
        url: '/getGrillDetail',
        method: 'post',
        data: { device_id:id }
    })
}

export function addGrill(params) {
    return request({
        url: '/addGrill',
        method: 'post',
        data: { ...params }
    })
}
