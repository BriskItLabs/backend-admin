import Amplify, { Storage } from 'aws-amplify'
import { getTimeFileName, changeStorageConfigure, getCompleteUrl, getKeyByCompleteUrl } from '@/utils/upload'
import { AMPLIFY_CONFIG } from '@/config/upload'

Amplify.configure(AMPLIFY_CONFIG)

const uploadSingleFile = async(urlConfigKey, file, callBack) => {
  const newStorage = changeStorageConfigure(Storage, urlConfigKey)
  const buketKey = getTimeFileName(file.type)
  const { key } = await newStorage.put(buketKey, file, {
    contentType: file.type,
    progressCallback(progress) {
      callBack(progress)
    }
  })
  return getCompleteUrl(urlConfigKey, key)
}

const getRemoteFile = (key) => {
  return Storage.get(key)
}

async function deleteSingleFile(urlConfigKey, completeUrl) {
  const newStorage = changeStorageConfigure(Storage, urlConfigKey)
  const buketKey = getKeyByCompleteUrl(completeUrl)
  return newStorage.remove(buketKey)
}

export {
  uploadSingleFile,
  getRemoteFile,
  deleteSingleFile
}
