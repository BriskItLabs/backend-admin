import request from '@/utils/request'

export function getOrderList() {
    return request({
        url: '/getOrderList',
        method: 'post',
    })
}

export function getOrderDetail(id) {
    return request({
        url: '/getOrderDetail',
        method: 'post',
        data: { order_id: id }
    })
}
