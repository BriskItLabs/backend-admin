import request from '@/utils/request'

export function getProductList(category) {
    return request({
        url: '/getProductList',
        method: 'post',
        data: { category: category }
    })
}

export function getProductDetail(id) {
    return request({
        url: '/getProductDetail',
        method: 'post',
        data: { product_id: id }
    })
}
