import request from '@/utils/request'

export function getProgramList() {
  return request({
    url: '/getProgramList',
    method: 'post',
    data: { params: {} }
  })
}
