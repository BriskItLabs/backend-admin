import request from '@/utils/request'

export function getModelList() {
  return request({
    url: '/getModelList',
    method: 'post',
  })
}

export function delModel(id) {
    return request({
      url: '/delModel',
      method: 'post',
      data: { model_id: id }
    })
}

export function addModel(params) {
  return request({
    url: '/addModel',
    method: 'post',
    data: { params: params }
  })
}

export function setModel(id,params) {
  return request({
    url: '/setModel',
    method: 'post',
    data: { model_id:id, params: params, }
  })
}

export function getModelDetail(id) {
  return request({
    url: '/getModelDetail',
    method: 'post',
    data: { model_id:id }
  })
}