import Amplify, { Auth } from 'aws-amplify'
import request from '@/utils/request'

Amplify.configure({
  Auth: {
    // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
    // identityPoolId: 'ap-southeast-1:1cb7782b-e98f-4aa7-996d-6eb8940e9311',
    identityPoolId: 'us-west-1:d58be424-ef53-46d5-812c-63b9650e6563',

    // REQUIRED - Amazon Cognito Region
    // region: 'ap-southeast-1',
    region: 'us-west-1',

    // OPTIONAL - Amazon Cognito Federated Identity Pool Region
    // Required only if it's different from Amazon Cognito Region
    // identityPoolRegion: 'ap-southeast-1',
    identityPoolRegion: 'us-west-1',

    // OPTIONAL - Amazon Cognito User Pool ID
    // userPoolId: 'ap-southeast-1_r68MeN4no',
    userPoolId: 'us-west-1_zF5pErVZj',
    // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
    // userPoolWebClientId: '5vruqlobck376p57hhdlkucsva',
    userPoolWebClientId: '1m9do46gvk3b6g9hb1t0n7ivvd',

    // oauth: {
    //   domain: 'http://smartgrilldevapp.auth.us-west-1.amazoncognito.com',
    //   redirectSignIn: 'myapp://',
    //   redirectSignOut: 'myapp://',
    //   responseType: 'token' // or 'token', note that REFRESH token will only be generated when the responseType is code
    // }

  }
})

export function login({ email, password }) {
  return Auth.signIn(email, password)
}

export function getInfo(token) {
  return Auth.currentAuthenticatedUser()
}

export function logout() {
  return Auth.signOut()
}

export function getAllUser() {
  return request({
    url: '/getAllUser',
    method: 'post',
  })
}

export function getProfile() {
  return request({
    url: '/getProfile',
    method: 'post',
  })
}
