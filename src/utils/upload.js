import { URL_CONFIG } from '@/config/upload'

function getTimeFileName(fileType) {
  const currentDate = new Date()
  const extension = getExtension(fileType)
  return `key-${currentDate.getTime()}.${extension}`
}

function getExtension(fileType) {
  return fileType.split('/')[1]
}

function getContentType(fileType) {
  return fileType.repalce('.', '/')
}

function getKeyByCompleteUrl(key) {
  return key.match(/(key-\d+\..+)$/)
}

function getCompleteUrl(urlConfigKey, buketKey) {
  const prefix = URL_CONFIG[urlConfigKey]
  return prefix + buketKey
}

function changeStorageConfigure(Storage, urlConfigKey) {
  const prefix = URL_CONFIG[urlConfigKey].match(/(\w+\/\w+\/)$/)[0]
  Storage.configure({
    customPrefix: {
      public: prefix
    }}
  )
  return Storage
}

export {
  getTimeFileName,
  getExtension,
  getContentType,
  getKeyByCompleteUrl,
  getCompleteUrl,
  changeStorageConfigure
}
