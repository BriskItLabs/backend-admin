const AMPLIFY_CONFIG = {
  // Auth: {
  //   // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
  //   identityPoolId: 'ap-southeast-1:670043ef-0900-4869-a89a-78468bbc5870',

  //   // REQUIRED - Amazon Cognito Region
  //   region: 'ap-southeast-1',

  //   // OPTIONAL - Amazon Cognito Federated Identity Pool Region
  //   // Required only if it's different from Amazon Cognito Region
  //   identityPoolRegion: 'ap-southeast-1',

  //   // OPTIONAL - Amazon Cognito User Pool ID
  //   userPoolId: 'ap-southeast-1_1hiGIqPHM',
  //   // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
  //   userPoolWebClientId: '57prki6o0e6h1clv8cos8pnrcd'

  // },
  Storage: {
    AWSS3: {
      bucket: 'bildev', // REQUIRED -  Amazon S3 bucket name
      region: 'ap-southeast-1' // OPTIONAL -  Amazon service region
    },
    customPrefix: {
      public: 'image/'
    }
  }
}

const URL_CONFIG = {
  RECIPE_IMAGE: 'https://bildev.s3-ap-southeast-1.amazonaws.com/image/recipe/',
  PRODUCT_IMAGE: 'https://bildev.s3-ap-southeast-1.amazonaws.com/image/product/',
  AVATAR_IMAGE: 'https://bildev.s3-ap-southeast-1.amazonaws.com/image/avatar/',
  RECIPE_VEDIO: 'https://bildev.s3-ap-southeast-1.amazonaws.com/video/recipe/',
  PRODUCT_VEDIO: 'https://bildev.s3-ap-southeast-1.amazonaws.com/video/product/'
}

export {
  AMPLIFY_CONFIG,
  URL_CONFIG
}
