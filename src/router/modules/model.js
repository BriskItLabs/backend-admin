import Layout from '@/layout'

export default {
  path: '/model',
  component: Layout,
  redirect: '/model/list',
  name: 'Model',
  meta: {
    title: 'Model'
  },
  children: [
    {
      path: 'list',
      name: 'ModelList',
      component: () => import('@/views/model/list'),
      meta: { title: 'Model', icon: 'list' },
      // hidden: true // 隐藏
    },
    {
      path: 'create',
      name: 'ModelCreate',
      component: () => import('@/views/model/create'),
      meta: { title: 'Create', icon: 'edit' },
      hidden: true
    },
    {
      path: 'edit/:id',
      name: 'ModelEdit',
      component: () => import('@/views/model/edit'),
      meta: { title: 'Edit', icon: 'edit', activeMenu: '/model/list' },
      hidden: true
    }
  ]
}
