import Layout from '@/layout'

export default {
  path: '/recipe',
  component: Layout,
  redirect: '/recipe/list',
  name: 'Recipe',
  meta: {
    title: 'Recipe'
  },
  children: [
    {
      path: 'list',
      name: 'RecipeList',
      component: () => import('@/views/recipe/list'),
      meta: { title: 'Recipe', icon: 'list' },
      // hidden: true // 隐藏
    },
    {
      path: 'create',
      name: 'RecipeCreate',
      component: () => import('@/views/recipe/create'),
      meta: { title: 'Create', icon: 'edit' },
      hidden: true,
    },
    {
      path: 'edit/:id',
      name: 'RecipeEdit',
      component: () => import('@/views/recipe/edit'),
      meta: { title: 'Edit', icon: 'edit', activeMenu: '/recipe/list' },
      hidden: true
    },
    {
      path: 'relation/:id',
      name: 'RecipeRelation',
      component: () => import('@/views/recipe/relation'),
      meta: { title: 'Relation', activeMenu: '/recipe/create' },
      hidden: true
    }
  ]
}
