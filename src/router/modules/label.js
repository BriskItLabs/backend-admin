import Layout from '@/layout'

export default {
  path: '/label',
  component: Layout,
  redirect: '/label/list',
  name: 'Label',
  meta: {
    title: 'Label'
  },
  children: [
    {
      path: 'list',
      name: 'LabelList',
      component: () => import('@/views/label/list'),
      meta: { title: 'Label', icon: 'list' }
    }
  ]
}