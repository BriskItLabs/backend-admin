import Layout from '@/layout'

export default {
  path: '/learn',
  component: Layout,
  redirect: '/learn/list',
  name: 'Learn',
  meta: {
    title: 'Learn'
  },
  children: [
    {
      path: 'list',
      name: 'LearnList',
      component: () => import('@/views/learn/list'),
      meta: { title: 'List', icon: 'list' }
    },
    {
      path: 'create',
      name: 'LearnCreate',
      component: () => import('@/views/learn/create'),
      meta: { title: 'Create', icon: 'edit' }
    },
    {
      path: 'edit/:id(\\d+)',
      name: 'LearnEdit',
      component: () => import('@/views/learn/edit'),
      meta: { title: 'Edit', icon: 'edit', activeMenu: '/learn/list' },
      hidden: true
    }
  ]
}
