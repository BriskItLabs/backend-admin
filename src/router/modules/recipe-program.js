import Layout from '@/layout'

export default {
  path: '/recipe-program',
  component: Layout,
  redirect: '/recipe-program/list',
  name: 'RecipeProgram',
  meta: {
    title: 'Recipe Program'
  },
  children: [
    {
      path: 'list',
      name: 'RecipeProgramList',
      component: () => import('@/views/recipe-program/list'),
      meta: { title: 'List', icon: 'list' }
    },
    {
      path: 'create',
      name: 'RecipeProgramCreate',
      component: () => import('@/views/recipe-program/create'),
      meta: { title: 'Create', icon: 'edit' }
    },
    {
      path: 'edit/:id',
      name: 'RecipeProgramEdit',
      component: () => import('@/views/recipe-program/edit'),
      meta: { title: 'Edit', icon: 'edit', activeMenu: '/recipe-program/list' },
      hidden: true
    }
  ]
}
