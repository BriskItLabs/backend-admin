import Layout from '@/layout'

export default {
  path: '/product',
  component: Layout,
  redirect: '/product/list',
  name: 'Product',
  meta: {
    title: 'Product'
  },
  children: [
    {
      path: 'list',
      name: 'ProductList',
      component: () => import('@/views/product/list'),
      meta: { title: 'List', icon: 'list' },
      hidden: true
    },
    {
      path: 'create',
      name: 'ProductCreate',
      component: () => import('@/views/product/create'),
      meta: { title: 'Create', icon: 'edit' },
      hidden: true
    },
    {
      path: 'edit/:id(\\d+)',
      name: 'ProductEdit',
      component: () => import('@/views/product/edit'),
      meta: { title: 'Edit', icon: 'edit', activeMenu: '/product/list' },
      hidden: true
    }
  ]
}
