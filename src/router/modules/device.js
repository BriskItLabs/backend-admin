import Layout from '@/layout'

export default {
  path: '/device',
  component: Layout,
  redirect: '/device/index',
  name: 'Device',
  meta: {
    title: 'Device'
  },
  children: [
    {
      path: 'index',
      name: 'device',
      component: () => import('@/views/device/index'),
      meta: { title: 'Device', icon: 'list' },
      // hidden: true // 隐藏
    },
    {
      path: 'create',
      name: 'DeviceCreate',
      component: () => import('@/views/device/create'),
      meta: { title: 'Create', icon: 'edit' },
      hidden: true
    },
  ]
}
