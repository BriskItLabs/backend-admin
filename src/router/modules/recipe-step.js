import Layout from '@/layout'

export default {
  path: '/recipe-step',
  component: Layout,
  redirect: '/recipe-step/list',
  name: 'RecipeStep',
  meta: {
    title: 'Recipe Step'
  },
  children: [
    {
      path: 'list',
      name: 'RecipeStepList',
      component: () => import('@/views/recipe-step/list'),
      meta: { title: 'List', icon: 'list' }
    },
    {
      path: 'create',
      name: 'RecipeStepCreate',
      component: () => import('@/views/recipe-step/create'),
      meta: { title: 'Create', icon: 'edit' }
    },
    {
      path: 'edit/:id(\\d+)',
      name: 'RecipeStepEdit',
      component: () => import('@/views/recipe-step/edit'),
      meta: { title: 'Edit', icon: 'edit', activeMenu: '/recipe-step/list' },
      hidden: true
    }
  ]
}
