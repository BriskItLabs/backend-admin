// 3-2-1 rib

//addRecipe 
params =  {
  name: '3-2-1 ribs',
  author_name: 'Diva Q',
  prep_time: 900000,
  cook_time: 21600000,
  difficulty: 'Intermediate',
  pellet_flavor: 'Hickory',
  category: 'Pork',
  meal_type: 'Dinner',
  is_origin: true,
  description: 'Our famously easy 3-2-1 ribs recipe will make your rib game the envy of the neighborhood. This super simple recipe takes all the confusion out of making ribs without sacrificing any flavor. Start by smoking your ribs for 3 hours, then cook inside foil for 2 hours and finish by removing from foil and brushing on sauce for up to an hour.'
}

step_1 = {
  index: 1,
  type: 0, // 0 表示prep step
  title: 'Remove membrane off',
  primary_desc: 'If your butcher has not already done so, remove the thin silverskin membrane from the bone-side of the ribs by working the tip of a butter knife or a screwdriver underneath the membrane over a middle bone. Use paper towels to get a firm grip, then tear the membrane off.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 0,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 0,
  timer2_duration: 0,
  is_manual_proceed: true,
  buffer_time: 0
}

step_2 = {
  index: 1,
  type: 2, // 2 表示auto cook step
  title: 'Preheat',
  primary_desc: 'When ready to cook, set Traeger temperature to 180℉ and preheat, lid closed for 15 minutes.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 180,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 900000,
  timer2_duration: 0,
  is_manual_proceed: false,
  buffer_time: 0
}

step_3 = {
  index: 2,
  type: 1, // 1 表示manual cook step
  title: 'Season the ribs',
  primary_desc: 'In a small bowl, combine the mustard, 1/4 cup of apple juice (reserve the rest) and the Worcestershire sauce. Spread the mixture thinly on both sides of the ribs and season with Traeger Pork & Poultry Rub.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 0,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 0,
  timer2_duration: 0,
  is_manual_proceed: true,
  buffer_time: 0
}

step_4 = {
  index: 3,
  type: 2, // 2 表示auto cook step
  title: 'Smoke the ribs',
  primary_desc: 'Smoke the ribs, meat-side up for 3 hours.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 180,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 10800000,
  timer2_duration: 0,
  is_manual_proceed: false,
  buffer_time: 0
}

step_5 = {
  index: 4,
  type: 1, // 1 表示manual cook step
  title: 'Season the ribs again',
  primary_desc: 'Tear off four long sheets of heavy-duty aluminum foil. Top with a rack of ribs and pull up the sides to keep the liquid enclosed. Sprinkle half the brown sugar on the rack, then top with half the honey and half the remaining apple juice. Use a bit more apple juice if you want more tender ribs. Lay another piece of foil on top and tightly crimp the edges so there is no leakage.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 0,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 0,
  timer2_duration: 0,
  is_manual_proceed: true,
  buffer_time: 0
}

step_6 = {
  index: 5,
  type: 2, // 2 表示auto cook step
  title: 'Cook foiled ribs',
  primary_desc: 'Return the foiled ribs to the grill and cook for an additional 2 hours.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 225,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 7200000,
  timer2_duration: 0,
  is_manual_proceed: true,
  buffer_time: 0
}

step_7 = {
  index: 6,
  type: 1, // 1 表示manual cook step
  title: 'Brush Traeger\'Que Sauce',
  primary_desc: 'Carefully remove the foil from the ribs and brush the ribs on both sides with Traeger\'Que Sauce. Discard the foil.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 0,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 0,
  timer2_duration: 0,
  is_manual_proceed: true,
  buffer_time: 0
}

step_7 = {
  index: 7,
  type: 2, // 2 表示auto cook step
  title: 'Continue to grill',
  primary_desc: 'Arrange the ribs directly on the grill grate and continue to grill until the sauce tightens, 30 to 60 minutes more.',
  secondary_desc: '',
  photo: '',
  video: '',
  ingredients: '',
  target_temp: 225,
  probe_a_temp: 0,
  probe_b_temp: 0,
  timer1_duration: 3600000,
  timer2_duration: 0,
  is_manual_proceed: true,
  buffer_time: 0
}